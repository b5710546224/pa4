package readability;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
/** 
 * This is a class for counting syllables of the word and counting the number of word 
 *  @author Natcha Pongsupanee 5710546224
 */
public class WordCounter {
	/** state to keep each type of letter */
	State state;
	/** variable counting number of syllables */
	int syllables,sentense =0;

	protected int totalSyl=0;
	protected int totalSen=0;
	protected int checkWord = 0;
	
	/** 
     * method to set type of state
     * @param new state that we want to set
     */
	public void setState(State newstate){
		if(newstate!=state) newstate.enterState();
		this.state = newstate;
	}
	
	  /** 
     * method checking if charactor is vowel
     * @param charactor that we want to check
     * @return true if c is vowel
     * 		   false if c is no vowel
     */
	public boolean isVowel(char c){
		return ("AIOUEyaioeuY".indexOf(c)>=0);
	}
	
	 /** 
     * method checking if charactor is letter
     * @param charactor that we want to check
     * @return true if c is letter
     * 		   false if c is no letter
     */
	public boolean isLetter(char c){
		return Character.isLetter(c);
	}
	
	 /** 
     * method checking if charactor is Dash or '
     * @param charactor that we want to check
     * @return true if c is Dash or '
     * 		   false if c is no Dash or '
     */
	public boolean isDash(char c){
		return ("-'".indexOf(c)>=0);
	}
	
	 /** 
     * method checking if charactor is e
     * @param charactor that we want to check
     * @return true if c is e
     * 		   false if c is no e
     */
	public boolean isE_FIRST(char c){
		return ("Ee".indexOf(c)>=0);
	}
	public boolean isEndSentense(char c){
		return (".,?".indexOf(c)>=0);
	}
	
	 /** E_FIRST state is a state for letter e*/
	State E_FIRST = new State(){
		public void handleChar(char c){
			if(isVowel(c)&&!("Yy".indexOf(c)>=0))setState(VOWELSTATE);
			else if(isE_FIRST(c));
			else if(isLetter(c)){
				syllables++;
				checkWord++;
				setState(CONSONANTSTATE);
			}
			else if(isDash(c))setState(DASHSTATE);
			else setState(NONWORDSTATE);
			
		}
		@Override
		public void enterState(){
			
		}
	};
	State END_SENT = new State(){
		public void handleChar(char c){
			if(isEndSentense(c));
			else if(isVowel(c)&&!("Yy".indexOf(c)>=0))setState(VOWELSTATE);
			else if(isE_FIRST(c));
			else if(isLetter(c)){
				syllables++;
				checkWord++;
				setState(CONSONANTSTATE);
			}
			else if(isDash(c))setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}
		@Override
		public void enterState(){
			if(checkWord>0){
				sentense++;
				checkWord=0;
			}
		}
	};
	
	 /** VOWEL state is a state for vowel*/
	State VOWELSTATE = new State(){
		public void handleChar(char c){
			if(isEndSentense(c))setState(END_SENT);
			else if(isVowel(c)&&!("Yy".indexOf(c)>=0));
			else if(isLetter(c))setState(CONSONANTSTATE);
			else if(isDash(c))setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}
		@Override
		public void enterState(){
			syllables++;
			checkWord++;
		}
	};
	
	 /** CONSONANT state is a state for all letter*/
	State CONSONANTSTATE = new State(){
		public void handleChar(char c){		
			if(isEndSentense(c))setState(END_SENT);
			else if(isE_FIRST(c))setState(E_FIRST);
			else if(isVowel(c))setState(VOWELSTATE);
			else if(isLetter(c));
			else if(isDash(c))setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}
		@Override
		public void enterState(){

		}
	};
	
	 /** START state is a state that be called first*/
	State STARTSTATE = new State(){
		public void handleChar(char c){
			if(isEndSentense(c))setState(END_SENT);
			else if(isVowel(c))setState(VOWELSTATE);
			else if(isE_FIRST(c))setState(E_FIRST);
			else if(isLetter(c))setState(CONSONANTSTATE);
			else if(isDash(c))setState(DASHSTATE);
			else setState(NONWORDSTATE);
		}
		public String toString() {return "START";};
		@Override
		public void enterState(){
	
		}
	};
	
	 /** DASH state is a state for letter -and '*/
	State DASHSTATE = new State(){
		public void handleChar(char c){
			if(isEndSentense(c))setState(END_SENT);
			else if(isVowel(c))setState(VOWELSTATE);
			else if(isE_FIRST(c))setState(E_FIRST);
			else if(isLetter(c))setState(CONSONANTSTATE);
			else if(isDash(c));
			else setState(NONWORDSTATE);
		}
		@Override
		public void enterState(){
		}
	};
	
	 /** NONWORD state is a state for charactor that's not belong to anytype*/
	State NONWORDSTATE = new State(){
		public void handleChar(char c){
	
		}
		@Override
		public void enterState(){
			syllables=0;
		}
	};
	
	 
	public int getSentense(){
		return this.sentense;
	}
	
	 /** 
     * method that call all state used to count the Syllables of the word
     * @param word is the word that we want to count
     */
	public int countSyllables(String word){
		state = STARTSTATE;
		syllables=0;
		for(int k=0;k<word.length();k++){
			char c = word.charAt(k);
			state.handleChar(c);
			if(k==0&&state == DASHSTATE)break;
		}
		if(state == DASHSTATE) return 0;
		
		else if(state == E_FIRST&&syllables==0) return 1;
	
		return syllables;
	}
	
	/** 
     * method that use the count the number of word
     * @param Input of text that we want to count
     * @return the number of word in text
     */
	public int countWords(InputStream instream) throws IOException{
		Scanner scanner = new Scanner(instream);

		int countWord = 0;
		while(scanner.hasNext()){
//			System.out.println("countWord");
			scanner.next();
			countWord++;
		}
		return countWord;
	}	
}
