package readability.Readability;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import readability.WordCounter;
import readability.WordCounterGUI;

public class Main {
	
	public static void main(String[] args) {
		String detail = "";
		int syllable=0,sentense=0,word=0;
		double readAbility;
		WordCounter wordcounter = new WordCounter();
		if(args.length>0){
			String FILE_URL = args[0];
			URL url = null;
			try {url = new URL(FILE_URL);
			} catch (MalformedURLException e) {e.printStackTrace();}
			
			InputStream input = null;
			InputStream input2 = null;
			try {input = url.openStream();
				input2 = url.openStream();
			} catch (IOException e) {e.printStackTrace();}
			
			Scanner scanner = new Scanner(input);
			
			
			WordCounter count = new WordCounter();
			scanner.useDelimiter("[\\s\\!\"():;]+");

			try {word = count.countWords(input2);
			} catch (IOException e) {e.printStackTrace();}
			
			while(scanner.hasNext()){
				syllable += count.countSyllables(scanner.next());}
				sentense = count.getSentense();
			
			readAbility = 206.835-84.6*(syllable*1.0/word)-1.015*(word*1.0/sentense);
			
			detail += String.format("%-40s%-40d\n", "Number of Syllables:",syllable);
			detail += String.format("%-40s%-40d\n", "Number of Words:",word);
			detail += String.format("%-40s%-40d\n", "Number of Sentences:",sentense);
			detail += String.format("%-40s%-40f\n", "Flesch Index:",readAbility);
			if(readAbility>=100) detail += String.format("%-40s:%40f\n", "Flesch Index",readAbility);
			else if(readAbility>=90) detail += String.format("%-40s%-40s\n", "Readability:","4th grade student");
			else if(readAbility>=80) detail += String.format("%-40s%-40s\n", "Readability:","5th grade student");
			else if(readAbility>=70) detail += String.format("%-40s%-40s\n", "Readability:","6th grade student");
			else if(readAbility>=65) detail += String.format("%-40s%-40s\n", "Readability:","7th grade student");
			else if(readAbility>=60) detail += String.format("%-40s%-40s\n", "Readability:","98th grade student");
			else if(readAbility>=50) detail += String.format("%-40s%-40s\n", "Readability:","High school student");
			else if(readAbility>=30) detail += String.format("%-40s%-40s\n", "Readability:","College student");
			else if(readAbility>=0) detail += String.format("%-40s:%-40s\n", "Readability:","College graduate");
			else detail += String.format("%-40s%-40s\n","Readability:", "Advanced degree graduate");
			System.out.print(detail);
			
		}
		else{
			WordCounterGUI gui = new WordCounterGUI();
		}
			
	}
}
