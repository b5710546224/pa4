package readability;
import java.io.IOException;
import java.io.InputStream;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class WordCounterGUI extends JFrame {
	JTextField textField1;
	String detail = "";
	JTextArea area;
	private JFrame frame = new JFrame();
	int syllable=0,sentense=0,word=0;
	double readAbility;
	/**
	 * The constructor for new ConverterUI
	 */
	public WordCounterGUI() {

		super("WordCounter");
		initComponents( );
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void initComponents(){

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

		JPanel firstRow = new JPanel();
		firstRow.setLayout(new FlowLayout());
		
		JLabel label1 = new JLabel("File or URL name: ");
		
		textField1 = new JTextField(15);
		
		JButton browse = new JButton("Browse");
		JButton count = new JButton("Count");
		JButton clear = new JButton("clear");
		
		area = new JTextArea(30,30);
		
		firstRow.add(label1);
		firstRow.add(textField1);
		firstRow.add(browse);
		firstRow.add(count);
		firstRow.add(clear);
		pane.add(firstRow);
		pane.add(area);
		
		contents.add(pane);
		
		ActionListener countListener = new CountButtonListener();
		count.addActionListener(countListener);
		
		ActionListener browseListener = new BrowseButtonListener();
		browse.addActionListener(browseListener);
		
		ActionListener clearListener = new ClearListener();
		clear.addActionListener(clearListener);
		
		this.pack();
		this.setVisible(true);
	}

	
	class ClearListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			textField1.setText("");
			area.setText("");
		}
	};	
	
	class BrowseButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(frame);
			textField1.setText("file:/"+ chooser.getSelectedFile().getAbsolutePath());
			
		}
	};	
	
	
	class CountButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String FILE_URL = textField1.getText();
			URL url = null;
			try {url = new URL(FILE_URL);
			} catch (MalformedURLException e) {e.printStackTrace();}
			
			InputStream input = null;
			InputStream input2 = null;
			try {input = url.openStream();
				input2 = url.openStream();
			} catch (IOException e) {e.printStackTrace();}
			
			Scanner scanner = new Scanner(input);
			
			
			WordCounter count = new WordCounter();
			scanner.useDelimiter("[\\s\\!\"():;]+");

			try {word = count.countWords(input2);
			} catch (IOException e) {e.printStackTrace();}
			
			while(scanner.hasNext()){
				syllable += count.countSyllables(scanner.next());}
				sentense = count.getSentense();
			
			readAbility = 206.835-84.6*(syllable*1.0/word)-1.015*(word*1.0/sentense);
			
			detail += String.format("%-40s%-40d\n", "Number of Syllables:",syllable);
			detail += String.format("%-40s%-40d\n", "Number of Words:",word);
			detail += String.format("%-40s%-40d\n", "Number of Sentences:",sentense);
			detail += String.format("%-40s%-40f\n", "Flesch Index:",readAbility);
			if(readAbility>=100) detail += String.format("%-40s:%40f\n", "Flesch Index",readAbility);
			else if(readAbility>=90) detail += String.format("%-40s%-40s\n", "Readability:","4th grade student");
			else if(readAbility>=80) detail += String.format("%-40s%-40s\n", "Readability:","5th grade student");
			else if(readAbility>=70) detail += String.format("%-40s%-40s\n", "Readability:","6th grade student");
			else if(readAbility>=65) detail += String.format("%-40s%-40s\n", "Readability:","7th grade student");
			else if(readAbility>=60) detail += String.format("%-40s%-40s\n", "Readability:","98th grade student");
			else if(readAbility>=50) detail += String.format("%-40s%-40s\n", "Readability:","High school student");
			else if(readAbility>=30) detail += String.format("%-40s%-40s\n", "Readability:","College student");
			else if(readAbility>=0) detail += String.format("%-40s:%-40s\n", "Readability:","College graduate");
			else detail += String.format("%-40s%-40s\n","Readability:", "Advanced degree graduate");
			//System.out.print(detail);
			area.setText(detail);
			
		}
	};	
}
